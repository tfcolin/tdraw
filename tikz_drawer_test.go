package tdraw

import (
	"testing"
)

func TestTDraw (t * testing.T) {

	loc := [][]float64{
		{0, 0},
		{2, 0},
		{1, 1},
		{3, 1},
		{2, 2},
		{1, 3},
		{3, 3},
		{2, 1},
		{1, 0.5},
		{2.5, 1.5},
		{0.5, 2},
		{2, 3},
		{3, 2},
		{1.3, 1.9},
	}

	bound := [][]float64 {
		{-0.5, -0.5},
		{3.5, 3.5},
	}

	d := InitTikzAniDrawer ("t", 3, bound)

	d.DrawLine (loc[8], loc[9], NoArrow, "iso line $1$", 0.3, 0)
	d.DrawLine (loc[9], loc[10], NormalArrow, "iso line $2$", 0.2, 1)
	d.DrawLine (loc[10], loc[8], LeftArrow, "", 0.1, 2)

	d.StartAnimate()

	d.MakeNode (loc[0], 0.3, []string{"N0", "$0,0$"}, Rectangle, 0)
	d.MakeNode (loc[1], 0.3, []string{"N1", "$2,0$"}, Circle, 1)
	d.MakeNode (loc[2], 0.3, []string{"N2", "$1,1$"}, Rectangle, 2)
	d.MakeNode (loc[3], 0.3, []string{"N3", "$3,1$"}, Circle, 3)
	d.MakeNode (loc[7], 0, []string{"Hello", "Fraction"}, TextBox, 3)

	d.DrawNodeLine (0, 2, NormalArrow, "$a\\rightarrow b$", 0.1, 3)
	d.DrawNodeLine (2, 1, LeftArrow, "", 0.15, 4)
	d.DrawNodeLine (3, 1, NoArrow, "$\\sin\\theta$", 0.2, 5)

	d.NextFrame()

	d.MakeNode (loc[4], 0.3, []string{"N4", "$2,2$"}, Circle, 0)
	d.MakeNode (loc[5], 0.3, []string{"N5", "$1,3$"}, Rectangle, 1)
	d.MakeNode (loc[6], 0.3, []string{"N6", "$3,3$"}, Circle, 2)
	d.MakeNode (loc[11], 0.1, []string{"N11", "$2,3$"}, Circle, 3)
	d.MakeNode (loc[12], 0, []string{"", ""}, Invisible, 0)

	d.DrawNodeLine (1, 0, NormalArrow, "$l_1$", 0.05, 1)
	d.DrawNodeLine (2, 0, NormalArrow, "$l_2$", 0.08, 2)
	d.DrawNodeLine (3, 4, NormalArrow, "$l_3$", 0.1, 3)
	d.DrawNodeLine (2, 3, NormalArrow, "$l_4$", 0.12, 4)
	d.DrawNodeLine (2, 4, NormalArrow, "$l_5$", 0.14, 5)

	d.DrawPoint (loc[13], 0.02, 3)

	d.Finish()
}
