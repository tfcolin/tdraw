package tdraw

import (
)

type Shape int 
const (
	Invisible Shape = 0 // valid parameters: loc
	Rectangle Shape = 1 // valid parameters: loc, size, text, style (at most 4 text)
	Circle Shape = 2 // valid parameters: loc, size, text, style (at most 2 text)
	TextBox Shape = 3 // valid parameters: loc, text (only 1 text)
)

type Arrow int
const (
	NoArrow Arrow = 0
	LeftArrow Arrow = 1
	NormalArrow Arrow = 2
)

type Drawer interface {
	MakeNode  (loc []float64, size float64, text []string, shape Shape, style int) (ind int)
	DrawNodeLine  (i1, i2 int, arrow Arrow, text string, width float64, style int)
	DrawLine (p1, p2 []float64, arrow Arrow, text string, width float64, style int) 
	DrawPoint (loc []float64, size float64, style int)
	GetNumStyle () int
	Finish ()
}

type AniDrawer interface {
	Drawer
	StartAnimate ()
	NextFrame ()
}
